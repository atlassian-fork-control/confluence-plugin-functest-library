package com.atlassian.confluence.plugin.functest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BuildInfo
{
    private static final Logger LOG = Logger.getLogger(BuildInfo.class);

    private static Properties BUILD_PROPERTIES = new Properties();

    static
    {
        InputStream buildPropertiesInput = BuildInfo.class.getClassLoader().getResourceAsStream("func-test.properties");
        if (null != buildPropertiesInput)
        {
            try
            {
                BUILD_PROPERTIES.load(buildPropertiesInput);
            }
            catch (IOException ioe)
            {
                LOG.error("Unable to read func-test.properties in the class path.");
            }
            finally
            {
                IOUtils.closeQuietly(buildPropertiesInput);
            }
        }
        else
        {
            LOG.warn("Unable to find the file func-test.properties in the class path.");
        }
    }

    public static String getGroupId()
    {
        return BUILD_PROPERTIES.getProperty("project.groupId");
    }

    public static String getArtifactId()
    {
        return BUILD_PROPERTIES.getProperty("project.artifactId");
    }

    public static String getVersion()
    {
        return BUILD_PROPERTIES.getProperty("project.version");
    }
}
