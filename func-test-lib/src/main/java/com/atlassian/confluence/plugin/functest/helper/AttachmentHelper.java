package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteAttachment;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;

public class AttachmentHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(AttachmentHelper.class);

    private long id;

    private long parentId;

    private String title;

    private String contentType;

    private String fileName;

    private long contentLength;

    private String comment;

    private String creator;

    private Date creationDate;

    private byte[] content;

    public AttachmentHelper(ConfluenceWebTester confluenceWebTester, final long parentId, final String fileName)
    {
        super(confluenceWebTester);
        setParentId(parentId);
        setFileName(fileName);
    }

    public AttachmentHelper(ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, 0, null);
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public long getParentId()
    {
        return parentId;
    }

    public void setParentId(long parentId)
    {
        this.parentId = parentId;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public long getContentLength()
    {
        return contentLength;
    }

    public void setContentLength(long contentLength)
    {
        this.contentLength = contentLength;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public byte[] getContent()
    {
        return content;
    }

    public void setContent(byte[] content)
    {
        this.content = content;
    }

    private RemoteAttachment toRemoteAttachment()
    {
        final RemoteAttachment remoteAttachment = new RemoteAttachment();
        final Calendar creationDate = Calendar.getInstance();

        remoteAttachment.setComment(getComment());

        remoteAttachment.setContentType(getContentType());

        if (null != getCreationDate())
        {
            creationDate.setTime(getCreationDate());
            remoteAttachment.setCreated(creationDate);
        }

        remoteAttachment.setCreator(getCreator());
        remoteAttachment.setFileName(getFileName());
        remoteAttachment.setFileSize(getContentLength());
        remoteAttachment.setId(getId());
        remoteAttachment.setPageId(getParentId());
        remoteAttachment.setTitle(getTitle());

        return remoteAttachment;
    }

    private void fromRemoteAttachment(final RemoteAttachment remoteAttachment)
    {
        setComment(remoteAttachment.getComment());
        setContentType(remoteAttachment.getContentType());
        setCreationDate(remoteAttachment.getCreated().getTime());
        setCreator(remoteAttachment.getCreator());
        setFileName(remoteAttachment.getFileName());
        setContentLength(remoteAttachment.getFileSize());
        setId(remoteAttachment.getId());
        setParentId(remoteAttachment.getPageId());
        setTitle(remoteAttachment.getTitle());
    }

    private byte[] getBytesFromContent() throws IOException
    {
        final ByteArrayOutputStream byteArrayOutputStream;

        if (null == getContent())
            return new byte[0];

        byteArrayOutputStream = new ByteArrayOutputStream((int) Math.max(0L, getContentLength()));
        IOUtils.copy(new ByteArrayInputStream(getContent()), byteArrayOutputStream);

        return byteArrayOutputStream.toByteArray();
    }

    public boolean create()
    {
        if (0 < getId())
            throw new IllegalStateException("The attachment already has an ID.");

        return saveOrUpdateAttachment();
    }

    private boolean saveOrUpdateAttachment()
    {
        String soapSessionToken = null;

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteAttachment remoteAttachment;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteAttachment = confluenceSoapService.addAttachment(soapSessionToken,
                    getParentId(), toRemoteAttachment(), getBytesFromContent());
            setId(remoteAttachment.getId());

            return getId() > 0;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean read()
    {
        String soapSessionToken = null;

        checkUpdatePrerequisites();

        try
        {
            final ConfluenceSoapService confluenceSoapService;
            final RemoteAttachment remoteAttachment;
            final boolean success;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            remoteAttachment = confluenceSoapService.getAttachment(soapSessionToken,
                    getParentId(), getFileName(), 0); /* Just return the latest version of the attachment */


            fromRemoteAttachment(remoteAttachment);

            if (success = (0 < getId()))
            {
                setContent(
                        confluenceSoapService.getAttachmentData(
                                soapSessionToken,
                                getParentId(),
                                getFileName(),
                                0));
            }

            return success;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    private void checkUpdatePrerequisites()
    {
        if (0 >= getParentId())
            throw new IllegalStateException("Parent ID not specified.");

        if (StringUtils.isBlank(getFileName()))
            throw new IllegalStateException("Attachment file name not specified.");
    }

    public boolean update()
    {
        return saveOrUpdateAttachment();
    }

    public boolean delete()
    {
        String soapSessionToken = null;

        checkUpdatePrerequisites();

        try
        {
            final ConfluenceSoapService confluenceSoapService;

            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.removeAttachment(
                    soapSessionToken, getParentId(), getFileName());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }
}
