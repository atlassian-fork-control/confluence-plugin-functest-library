package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

public class BandanaHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(BandanaHelper.class);

    private String bandanaKey;

    private String spaceKey = StringUtils.EMPTY;

    private String value;

    public BandanaHelper(final ConfluenceWebTester confluenceWebTester, final String spaceKey, final String key)
    {
        super(confluenceWebTester);
        setSpaceKey(spaceKey);
        setBandanaKey(key);
    }

    public BandanaHelper(final ConfluenceWebTester confluenceWebTester, final String key)
    {
        this(confluenceWebTester, StringUtils.EMPTY, key);
    }

    public String getBandanaKey()
    {
        return bandanaKey;
    }

    public void setBandanaKey(String bandanaKey)
    {
        this.bandanaKey = bandanaKey;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean create()
    {
        return update();
    }

    public boolean read()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();
            final Map bandanaEntry;

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            bandanaEntry = (Map) xmlRpcClient.execute("functest-bandana.getValue",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    StringUtils.defaultString(getSpaceKey()),
                                    StringUtils.defaultString(getBandanaKey())
                            )
                    ));

            setSpaceKey((String) bandanaEntry.get("spaceKey"));
            setBandanaKey((String) bandanaEntry.get("key"));
            setValue((String) bandanaEntry.get("value"));

            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return false;
    }

    public boolean update()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (Boolean) xmlRpcClient.execute("functest-bandana.setValue",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    StringUtils.defaultString(getSpaceKey()),
                                    StringUtils.defaultString(getBandanaKey()),
                                    StringUtils.defaultString(getValue())
                            )
                    ));

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return false;
    }

    public boolean delete()
    {
        String authenticationToken = null;

        try
        {
            final XmlRpcClient xmlRpcClient = confluenceWebTester.getXmlRpcClient();

            authenticationToken = confluenceWebTester.loginToXmlRPcService();
            return (Boolean) xmlRpcClient.execute("functest-bandana.removeValue",
                    new Vector<String>(
                            Arrays.asList(
                                    authenticationToken,
                                    StringUtils.defaultString(getSpaceKey()),
                                    StringUtils.defaultString(getBandanaKey())
                            )
                    ));

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final XmlRpcException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromXmlRpcService(authenticationToken);
        }

        return false;
    }
}
