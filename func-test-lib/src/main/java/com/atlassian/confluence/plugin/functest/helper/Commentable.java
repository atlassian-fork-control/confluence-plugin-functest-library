package com.atlassian.confluence.plugin.functest.helper;

import java.util.List;

public interface Commentable
{

    List<Long> getCommentIds();
}
