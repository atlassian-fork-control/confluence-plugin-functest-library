package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

public class HelperFactory
{

    public static AttachmentHelper createAttachmentHelper(ConfluenceWebTester confluenceWebTester, long parentId, String fileName)
    {
        return null != fileName && parentId > 0
                ? new AttachmentHelper(confluenceWebTester, parentId, fileName)
                : new AttachmentHelper(confluenceWebTester);
    }

    public static AttachmentHelper createAttachmentHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createAttachmentHelper(confluenceWebTester, 0, null);
    }

    public static BandanaHelper createBandanaHelper(ConfluenceWebTester confluenceWebTester, String contextKey, String bandanaKey)
    {
        return null != bandanaKey
                ? new BandanaHelper(confluenceWebTester, contextKey, bandanaKey)
                : new BandanaHelper(confluenceWebTester, contextKey);
    }

    public static BandanaHelper createBandanaHelper(ConfluenceWebTester confluenceWebTester, String contextKey)
    {
        return createBandanaHelper(confluenceWebTester, contextKey, null);
    }

    public static BlogPostHelper createBlogPostHelper(ConfluenceWebTester confluenceWebTester, long blogPostId)
    {
        return 0 < blogPostId
                ? new BlogPostHelper(confluenceWebTester, blogPostId)
                : new BlogPostHelper(confluenceWebTester);
    }

    public static BlogPostHelper createBlogPostHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createBlogPostHelper(confluenceWebTester, 0);
    }

    public static CommentHelper createCommentHelper(ConfluenceWebTester confluenceWebTester, long commentId)
    {
        return 0 < commentId
                ? new CommentHelper(confluenceWebTester, commentId)
                : new CommentHelper(confluenceWebTester);
    }

    public static CommentHelper createCommentHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createCommentHelper(confluenceWebTester, 0);
    }

    public static GroupHelper createGrouphelper(ConfluenceWebTester confluenceWebTester, String groupName)
    {
        return null != groupName
                ? new GroupHelper(confluenceWebTester, groupName)
                : new GroupHelper(confluenceWebTester);
    }

    public static GroupHelper createGrouphelper(ConfluenceWebTester confluenceWebTester)
    {
        return createGrouphelper(confluenceWebTester, null);
    }

    public static IndexHelper createIndexHelper(ConfluenceWebTester confluenceWebTester)
    {
        return new IndexHelper(confluenceWebTester);
    }

    public static MailServerHelper createMailServerHelper(ConfluenceWebTester confluenceWebTester, long mailServerId)
    {
        return 0 < mailServerId
                ? new MailServerHelper(confluenceWebTester, mailServerId)
                : new MailServerHelper(confluenceWebTester);
    }

    public static MailServerHelper createMailServerHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createMailServerHelper(confluenceWebTester, 0);
    }

    public static PageHelper createPageHelper(ConfluenceWebTester confluenceWebTester, long pageId)
    {
        return 0 < pageId
                ? new PageHelper(confluenceWebTester, pageId)
                : new PageHelper(confluenceWebTester);
    }

    public static PageHelper createPageHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createPageHelper(confluenceWebTester, 0);
    }

    public static SpaceHelper createSpaceHelper(ConfluenceWebTester confluenceWebTester, String spaceKey)
    {
        return null != spaceKey
                ? new SpaceHelper(confluenceWebTester, spaceKey)
                : new SpaceHelper(confluenceWebTester);
    }

    public static SpaceHelper createSpaceHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createSpaceHelper(confluenceWebTester, null);
    }

    public static UserHelper createUserHelper(ConfluenceWebTester confluenceWebTester, String userName)
    {
        return null != userName
                ? new UserHelper(confluenceWebTester, userName)
                : new UserHelper(confluenceWebTester);
    }

    public static UserHelper createUserHelper(ConfluenceWebTester confluenceWebTester)
    {
        return createUserHelper(confluenceWebTester, null);
    }
}
