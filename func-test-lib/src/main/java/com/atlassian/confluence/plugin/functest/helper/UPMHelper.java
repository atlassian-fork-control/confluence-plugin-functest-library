package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.TesterConfiguration;
import com.atlassian.confluence.plugin.functest.util.PluginMetadataUtil;
import com.atlassian.json.jsonorg.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class UPMHelper
{
    private static final Logger LOGGER = Logger.getLogger(UPMHelper.class);
    private static final int SLEEP_TIME = 1000;
    private static final int TRYING = 5;
    private static final String UPM_TOKEN_HEADER = "upm-token";

    public static String buildUPMBaseURL(String confluenceBaseURL)
    {
        if (!confluenceBaseURL.endsWith("/"))
        {
            confluenceBaseURL += "/";
        }
        confluenceBaseURL += "rest/plugins/1.0/";
        return confluenceBaseURL;
    }

    public static String buildUPMBaseURLWithToken(final String confluenceBaseURL, final String token)
    {
        return buildUPMBaseURL(confluenceBaseURL) + "?token=" + token;
    }

    public static String buildUPMBaseURLWithPluginKey(final String confluenceBaseURL, final String pluginKey)
    {
        return buildUPMBaseURL(confluenceBaseURL) + pluginKey;
    }

    public static String buildUPMBaseURLWithPluginKeyAndModule(final String confluenceBaseURL, final String pluginKey, final String moduleKey)
    {
        return buildUPMBaseURL(confluenceBaseURL) + pluginKey + "/modules/" + moduleKey;
    }

    public static String buildUPMPluginOrModuleKey(final String pluginOrModuleKey)
    {
        return pluginOrModuleKey + "-key";
    }

    public static HttpClientContext buildHttpClientContext(final TesterConfiguration testerConfiguration)
    {
        final CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new org.apache.http.auth.AuthScope(org.apache.http.auth.AuthScope.ANY_HOST
                        , org.apache.http.auth.AuthScope.ANY_PORT),
                new org.apache.http.auth.UsernamePasswordCredentials(testerConfiguration.getAdminUserName(), testerConfiguration.getAdminPassword()));

        final HttpHost targetHost = new HttpHost(testerConfiguration.getHostName(), testerConfiguration.getPort(), "http");
        final AuthCache authCache = new BasicAuthCache();
        final BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);
        final HttpClientContext httpClientContext = HttpClientContext.create();
        httpClientContext.setCredentialsProvider(credsProvider);
        httpClientContext.setAuthCache(authCache);

        return httpClientContext;
    }

    public static String getXSRFToken(final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        final CloseableHttpClient client = HttpClients.createDefault();
        final String upmBaseURL = buildUPMBaseURL(testerConfiguration.getBaseUrl());
        final HttpHead httphead = new HttpHead(upmBaseURL);

        CloseableHttpResponse response = null;
        try
        {
            response = client.execute(httphead, httpClientContext);
            final Header firstHeader = response.getFirstHeader(UPM_TOKEN_HEADER);
            if (firstHeader == null)
                throw new IllegalStateException("No header '" + UPM_TOKEN_HEADER + "' returned in " + response.getStatusLine().getStatusCode() + " response from " + upmBaseURL);
            return firstHeader.getValue();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            closeQuietly(response);
            closeQuietly(client);
        }
    }

    public static boolean installPlugin(final File pluginJar, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;

        final CloseableHttpClient client = HttpClients.createDefault();
        final String token = getXSRFToken(testerConfiguration, httpClientContext);

        final HttpPost postRequest = new HttpPost(buildUPMBaseURLWithToken(testerConfiguration.getBaseUrl(), token));
        CloseableHttpResponse response = null;
        try
        {

            final MultipartEntityBuilder multiPartEntity = MultipartEntityBuilder.create();
            final FileBody fileBody = new FileBody(pluginJar, ContentType.APPLICATION_OCTET_STREAM);
            multiPartEntity.addPart("plugin", fileBody);
            postRequest.setEntity(multiPartEntity.build());
            response = client.execute(postRequest, httpClientContext);
            if (response != null)
            {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 202)
                {
                    String pluginKey = PluginMetadataUtil.getPluginKey(
                            PluginMetadataUtil.getPluginDescriptorDom(pluginJar)
                    );
                    pluginKey = UPMHelper.buildUPMPluginOrModuleKey(pluginKey);
                    result = isPluginInstalled(pluginKey, testerConfiguration, httpClientContext);
                }
            }
        }
        catch (ParserConfigurationException e)
        {
            LOGGER.error("", e);
        }
        catch (SAXException e)
        {
            LOGGER.error("", e);
        }
        catch (IOException e)
        {
            LOGGER.error("", e);
        }
        catch (XPathExpressionException e)
        {
            LOGGER.error("", e);
        }
        finally
        {
            closeQuietly(response);
            closeQuietly(client);
        }

        return result;
    }

    public static boolean enablePlugin(final String pluginKey, boolean enabled, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPut putRequest = new HttpPut(buildUPMBaseURLWithPluginKey(testerConfiguration.getBaseUrl(), pluginKey));
        CloseableHttpResponse response = null;
        try
        {
            StringEntity entity = new StringEntity("{\"enabled\":" + enabled + "}");
            entity.setContentType("application/vnd.atl.plugins.plugin+json");

            putRequest.setEntity(entity);
            response = client.execute(putRequest, httpClientContext);
            if (response != null)
            {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    JSONObject obj = new JSONObject(IOUtils.toString(response.getEntity().getContent()));
                    result = obj.getBoolean("enabled");
                }
            }
        }
        catch (IOException e)
        {
            LOGGER.error("", e);
        }
        finally
        {
            closeQuietly(response);
            closeQuietly(client);
        }

        return result;
    }

    public static boolean enablePluginModule(final String pluginKey, final String moduleKey, boolean enabled, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPut putRequest = new HttpPut(buildUPMBaseURLWithPluginKeyAndModule(testerConfiguration.getBaseUrl(), pluginKey, moduleKey));
        CloseableHttpResponse response = null;
        try
        {
            StringEntity entity = new StringEntity("{\"enabled\":" + enabled + "}");
            entity.setContentType("application/vnd.atl.plugins.plugin.module+json");

            putRequest.setEntity(entity);
            response = client.execute(putRequest, httpClientContext);
            if (response != null)
            {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    JSONObject obj = new JSONObject(IOUtils.toString(response.getEntity().getContent()));
                    result = obj.getBoolean("enabled");
                }
            }
        }
        catch (IOException e)
        {
            LOGGER.error("", e);
        }
        finally
        {
            closeQuietly(response);
            closeQuietly(client);
        }

        return result;
    }

    public static boolean isPluginModuleEnabled(final String pluginKey, final String moduleKey, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;
        try
        {
            final HttpGet getRequest = new HttpGet(buildUPMBaseURLWithPluginKeyAndModule(testerConfiguration.getBaseUrl(), pluginKey, moduleKey));
            int numberOfTrying = 0;
            boolean responseResult = false;
            while (!responseResult)
            {
                final CloseableHttpClient client = HttpClients.createDefault();
                final CloseableHttpResponse response = client.execute(getRequest, httpClientContext);
                try
                {
                    if (response != null)
                    {
                        final int statusCode = response.getStatusLine().getStatusCode();
                        responseResult = (statusCode == 200);
                        if (!responseResult)
                        {
                            if (++numberOfTrying > TRYING)
                            {
                                break;
                            }
                            Thread.sleep(SLEEP_TIME);
                        }
                        else
                        {
                            HttpEntity entity = response.getEntity();
                            InputStream inputStream = entity.getContent();
                            String content = IOUtils.toString(inputStream);
                            JSONObject a = new JSONObject(content);
                            result = a.getBoolean("enabled");
                        }
                    }
                }
                finally
                {
                    closeQuietly(response);
                    closeQuietly(client);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.error("", e);
        }
        return result;
    }

    public static boolean isPluginEnabled(final String pluginKey, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;
        try
        {
            final HttpGet getRequest = new HttpGet(buildUPMBaseURLWithPluginKey(testerConfiguration.getBaseUrl(), pluginKey));
            int numberOfTrying = 0;
            boolean responseResult = false;
            while (!responseResult)
            {
                final CloseableHttpClient client = HttpClients.createDefault();
                final CloseableHttpResponse response = client.execute(getRequest, httpClientContext);
                try
                {
                    if (response != null)
                    {
                        final int statusCode = response.getStatusLine().getStatusCode();
                        responseResult = (statusCode == 200);
                        if (!responseResult)
                        {
                            if (++numberOfTrying > TRYING)
                            {
                                break;
                            }
                            Thread.sleep(SLEEP_TIME);
                        }
                        else
                        {
                            HttpEntity entity = response.getEntity();
                            InputStream inputStream = entity.getContent();
                            String content = IOUtils.toString(inputStream);
                            JSONObject a = new JSONObject(content);
                            result = a.getBoolean("enabled");
                        }
                    }
                }
                finally
                {
                    closeQuietly(response);
                    closeQuietly(client);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.error("", e);
        }
        return result;
    }

    public static boolean isPluginInstalled(String pluginKey, TesterConfiguration testerConfiguration, HttpClientContext httpClientContext)
    {
        boolean result = false;
        try
        {
            final HttpGet getRequest = new HttpGet(buildUPMBaseURLWithPluginKey(testerConfiguration.getBaseUrl(), pluginKey));
            int numberOfTrying = 0;
            while (!result)
            {
                final CloseableHttpClient client = HttpClients.createDefault();
                final CloseableHttpResponse response = client.execute(getRequest, httpClientContext);
                try
                {

                    if (response != null)
                    {
                        final int statusCode = response.getStatusLine().getStatusCode();
                        result = (statusCode == 200);
                        if (!result)
                        {
                            if (++numberOfTrying > TRYING)
                            {
                                break;
                            }
                            Thread.sleep(SLEEP_TIME);
                        }
                    }
                }
                finally
                {
                    closeQuietly(response);
                    closeQuietly(client);
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.error("", e);
        }
        return result;
    }

    public static boolean uninstallPlugin(final String pluginKey, final TesterConfiguration testerConfiguration, final HttpClientContext httpClientContext)
    {
        boolean result = false;

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpDelete deleteRequest = new HttpDelete(buildUPMBaseURLWithPluginKey(testerConfiguration.getBaseUrl(), pluginKey));
        CloseableHttpResponse response = null;
        try
        {
            response = client.execute(deleteRequest, httpClientContext);
            if (response != null)
            {
                int statusCode = response.getStatusLine().getStatusCode();
                result = (statusCode == 204);
            }
        }
        catch (IOException e)
        {
            LOGGER.error("", e);
        }
        finally
        {
            closeQuietly(response);
            closeQuietly(client);
        }

        return result;
    }

    private static void closeQuietly(Closeable obj)
    {
        try
        {
            obj.close();
        }
        catch (IOException e)
        {
            //quietly
        }
    }
}