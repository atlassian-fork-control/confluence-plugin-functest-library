package com.atlassian.confluence.plugin.functest.helper;

import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.ConfluenceSoapService;
import com.atlassian.confluence.plugin.functest.remote.soap.stub.RemoteUser;
import org.apache.log4j.Logger;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserHelper extends AbstractHelper
{

    private static final Logger LOG = Logger.getLogger(UserHelper.class);

    private String name;

    private String fullName;

    private String emailAddress;

    private String password;

    private List<String> groups;

    public UserHelper(final ConfluenceWebTester confluenceWebTester, final String name)
    {
        super(confluenceWebTester);
        setName(name);
        setGroups(new ArrayList<String>());
    }

    public UserHelper(final ConfluenceWebTester confluenceWebTester)
    {
        this(confluenceWebTester, null);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public List<String> getGroups()
    {
        return groups;
    }

    public void setGroups(List<String> groups)
    {
        this.groups = groups;
    }

    protected RemoteUser toRemoteUser()
    {
        final RemoteUser remoteUser = new RemoteUser();

        remoteUser.setName(getName());
        remoteUser.setFullname(getFullName());
        remoteUser.setEmail(getEmailAddress());

        return remoteUser;
    }

    protected void populateHelper(final RemoteUser remoteUser)
    {
        setName(remoteUser.getName());
        setFullName(remoteUser.getFullname());
        setEmailAddress(remoteUser.getEmail());
    }

    protected void addUserToGroups(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws RemoteException
    {
        for (String groupName : getGroups())
        {
            confluenceSoapService.addUserToGroup(soapSessionToken, getName(), groupName);
        }
    }

    protected void removeUserGroups(
            final String soapSessionToken,
            final ConfluenceSoapService confluenceSoapService) throws RemoteException
    {
        for (String groupName : Arrays.asList(confluenceSoapService.getUserGroups(soapSessionToken, getName())))
        {
            confluenceSoapService.removeUserFromGroup(soapSessionToken, getName(), groupName);
        }
    }

    public boolean create()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            confluenceSoapService.addUser(soapSessionToken, toRemoteUser(), getPassword());
            addUserToGroups(soapSessionToken, confluenceSoapService);
            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean updatePassword()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.changeUserPassword(soapSessionToken, getName(), getPassword());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean read()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            populateHelper(confluenceSoapService.getUser(soapSessionToken, getName()));
            setGroups(Arrays.asList(confluenceSoapService.getUserGroups(soapSessionToken, getName())));

            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean update()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            confluenceSoapService.editUser(soapSessionToken, toRemoteUser());
            removeUserGroups(soapSessionToken, confluenceSoapService);
            addUserToGroups(soapSessionToken, confluenceSoapService);
            return true;

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }

    public boolean delete()
    {
        String soapSessionToken = null;
        ConfluenceSoapService confluenceSoapService;

        try
        {
            soapSessionToken = confluenceWebTester.loginToSoapService();
            confluenceSoapService = confluenceWebTester.getConfluenceSoapService();

            return confluenceSoapService.removeUser(soapSessionToken, getName());

        }
        catch (final MalformedURLException mUrlE)
        {
            LOG.error("Invalid RPC URL specified.", mUrlE);
        }
        catch (final ServiceException se)
        {
            LOG.error("Service request denied.", se);
        }
        catch (final RemoteException re)
        {
            LOG.error("There's an error in Confluence.", re);
        }
        catch (final IOException ioe)
        {
            LOG.error("Can't talk to Confluence.", ioe);
        }
        finally
        {
            confluenceWebTester.logoutFromSoapService(soapSessionToken);
        }

        return false;
    }
}
