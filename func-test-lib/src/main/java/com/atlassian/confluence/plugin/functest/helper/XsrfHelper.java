package com.atlassian.confluence.plugin.functest.helper;

import net.sourceforge.jwebunit.junit.WebTester;
import com.atlassian.confluence.plugin.functest.ConfluenceWebTester;

/**
 * XSRF assertions helper?
 *
 * @deprecated
 * See {@link #assertTokenValidationFails(net.sourceforge.jwebunit.junit.WebTester, String)}
 */
public class XsrfHelper
{
    /**
     * Asserts that a resource is XSRF protected.
     *
     * @param tester
     * An instance of {@link com.atlassian.confluence.plugin.functest.ConfluenceWebTester}.
     * @param resourcePath
     * The resource to test for XSRF protection. It must not contain the XSRF token. Must be absolute (e.g. <tt>/admin/plugins.action</tt>).
     *
     * @throws IllegalArgumentException
     * Thrown if the specified tester is not an instance of {@link com.atlassian.confluence.plugin.functest.ConfluenceWebTester}
     *
     * @deprecated
     * Please use {@link com.atlassian.confluence.plugin.functest.ConfluenceWebTester#assertResourceXsrfProtected(String)} instead.
     */
    public static void assertTokenValidationFails(WebTester tester, String resourcePath)
    {
        if (!(tester instanceof ConfluenceWebTester))
            throw new IllegalArgumentException("Specified tester not a " + ConfluenceWebTester.class.getName());

        ((ConfluenceWebTester) tester).assertResourceXsrfProtected(resourcePath);
    }
}
