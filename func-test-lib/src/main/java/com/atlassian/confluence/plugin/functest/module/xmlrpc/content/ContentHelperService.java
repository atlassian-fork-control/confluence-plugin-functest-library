package com.atlassian.confluence.plugin.functest.module.xmlrpc.content;

import com.atlassian.confluence.rpc.SecureRpc;

public interface ContentHelperService extends SecureRpc
{
    boolean addWatch(String authenticationToken, String entityId);

    boolean removeWatch(String authenticationToken, String entityId);

    boolean isWatched(String authenticationToken, String entityId);

    boolean markFavorite(String authenticationToken, String entityId);

    boolean unmarkFavorite(String authentictionToken, String entityId);

    boolean isFavorite(String authenticationToken, String entityId);
}
