package com.atlassian.confluence.plugin.functest.module.xmlrpc.index;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;


public class IndexHelperServiceImpl implements IndexHelperService
{
    private IndexHelperService indexHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    public void setIndexHelperServiceDelegate(IndexHelperService indexHelperServiceDelegate)
    {
        this.indexHelperServiceDelegate = indexHelperServiceDelegate;
    }

    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public Boolean reindex(final String authenticationToken)
    {
        // No outer transaction required for Confluence 5.2 onwards due to the searchableDao getting its own
        // transaction.
        // In fact, an outer transaction lasting the duration of the multi-threaded indexing operation will
        // cause CountDownLatch hangs in HSQLDB.
        return indexHelperServiceDelegate.reindex(authenticationToken);
    }

    public Boolean flush(final String authenticationToken)
    {
        return (Boolean) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return indexHelperServiceDelegate.flush(authenticationToken);
                    }
                }
        );
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
