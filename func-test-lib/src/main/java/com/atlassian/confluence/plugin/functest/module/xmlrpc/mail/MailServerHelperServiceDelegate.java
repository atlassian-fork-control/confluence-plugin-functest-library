package com.atlassian.confluence.plugin.functest.module.xmlrpc.mail;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.jmx.JmxSMTPMailServer;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaKeys;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;

import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class MailServerHelperServiceDelegate implements MailServerHelperService
{

    private PermissionManager permissionManager;

    private BandanaManager bandanaManager;

    public MailServerManager getMailServerManager()
    {
        return MailFactory.getServerManager();
    }

    public PermissionManager getPermissionManager()
    {
        return permissionManager;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public BandanaManager getBandanaManager()
    {
        return bandanaManager;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

    private boolean isCurrentUserAnAdmin()
    {
        final User user = AuthenticatedUserThreadLocal.get();

        return null != user && getPermissionManager().hasPermission(
                user,
                Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION);
    }

    private SMTPMailServer toSMTPMailServer(final Map mailServerStructure) throws RemoteException
    {
        final String[] smtpServerAddressParts;
        final SMTPMailServer smtpMailServer = new JmxSMTPMailServer(
                Long.valueOf((String) mailServerStructure.get("id")),
                (String) mailServerStructure.get("name"),
                null,
                (String) mailServerStructure.get("fromAddress"),
                (String) mailServerStructure.get("prefix"),
                isNotBlank((String) mailServerStructure.get("jndiLocation")),
                (String) mailServerStructure.get("jndiLocation"),
                (String) mailServerStructure.get("userName"),
                (String) mailServerStructure.get("password")
        );

        smtpServerAddressParts = StringUtils.split((String) mailServerStructure.get("address"), ':');
        if (smtpServerAddressParts.length == 1)
        {
            smtpMailServer.setHostname(smtpServerAddressParts[0]);
            smtpMailServer.setPort(SMTPMailServer.DEFAULT_SMTP_PORT);
        }
        else if (smtpServerAddressParts.length == 2)
        {
            smtpMailServer.setHostname(smtpServerAddressParts[0]);
            smtpMailServer.setPort(smtpServerAddressParts[1]);
        }
        else
        {
            throw new RemoteException("Invalid address specified: " + mailServerStructure.get("address"));
        }

        return smtpMailServer;
    }

    private Hashtable<String, String> toHashtable(final SMTPMailServer smtpMailServer) throws RemoteException
    {
        final Hashtable<String, String> hashtable = new Hashtable<>();

        if (null != smtpMailServer.getId())
        {
            hashtable.put("id", String.valueOf(smtpMailServer.getId()));
        }
        if (null != smtpMailServer.getName())
        {
            hashtable.put("name", smtpMailServer.getName());
        }
        if (null != smtpMailServer.getDefaultFrom())
        {
            hashtable.put("fromAddress", smtpMailServer.getDefaultFrom());
        }
        if (null != smtpMailServer.getPrefix())
        {
            hashtable.put("prefix", smtpMailServer.getPrefix());
        }
        if (null != smtpMailServer.getJndiLocation())
        {
            hashtable.put("jndiLocation", smtpMailServer.getJndiLocation());
        }
        if (null != smtpMailServer.getUsername())
        {
            hashtable.put("userName", smtpMailServer.getUsername());
        }
        if (null != smtpMailServer.getPassword())
        {
            hashtable.put("password", smtpMailServer.getPassword());
        }
        if (null != smtpMailServer.getHostname() && null != smtpMailServer.getPort())
        {
            hashtable.put("address", smtpMailServer.getHostname() + ":" + smtpMailServer.getPort());
        }

        return hashtable;
    }

    private void validateMailServer(SMTPMailServer smtpMailServer) throws RemoteException
    {
        if (StringUtils.isBlank(smtpMailServer.getName()))
        {
            throw new RemoteException("Mail server name not specified.");
        }
        if (StringUtils.isBlank(smtpMailServer.getDefaultFrom()))
        {
            throw new RemoteException("From address not specified.");
        }
        if (StringUtils.isBlank(smtpMailServer.getPrefix()))
        {
            throw new RemoteException("Prefix not specified.");
        }
        if (StringUtils.isBlank(smtpMailServer.getHostname()) && StringUtils.isBlank(smtpMailServer.getJndiLocation()))
        {
            throw new RemoteException("Server address/JNDI location not specified.");
        }
    }

    private Map<Long, SMTPMailServer> getMailServersMap()
    {
        @SuppressWarnings("unchecked")
        Map<Long, SMTPMailServer> mailServersMap = (Map<Long, SMTPMailServer>) getBandanaManager().getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ConfluenceBandanaKeys.MAIL_ACCOUNTS);
        if (null == mailServersMap)
        {
            mailServersMap = new LinkedHashMap<>();
        }
        return mailServersMap;
    }

    private void persistMailServersMap(Map<Long, SMTPMailServer> mailServersMap)
    {
        getBandanaManager().setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ConfluenceBandanaKeys.MAIL_ACCOUNTS, mailServersMap);
    }

    public String createMailServer(final String authToken, final Hashtable mailServerStructure) throws RemoteException
    {
        final SMTPMailServer smtpMailServer;

        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        smtpMailServer = toSMTPMailServer(mailServerStructure);

        validateMailServer(smtpMailServer);

        smtpMailServer.setId(System.currentTimeMillis());

        Map<Long, SMTPMailServer> mailServersMap = getMailServersMap();
        mailServersMap.put(smtpMailServer.getId(), smtpMailServer);
        persistMailServersMap(mailServersMap);

        return smtpMailServer.getId().toString();
    }

    public boolean updateMailServer(final String authToken, final Hashtable mailServerStructure) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        SMTPMailServer smtpMailServer = toSMTPMailServer(mailServerStructure);
        Long mailServerId = smtpMailServer.getId();
        Map<Long, SMTPMailServer> mailServersMap = getMailServersMap();

        mailServersMap.put(mailServerId, smtpMailServer);
        persistMailServersMap(mailServersMap);

        return true;
    }

    public Hashtable<String, String> readMailServer(final String authToken, final String mailServerId) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }

        Long mailServerIdLong = Long.valueOf(mailServerId);
        Map mailServersMap = getMailServersMap();

        SMTPMailServer smtpMailServer = (SMTPMailServer) mailServersMap.get(mailServerIdLong);
        return null != smtpMailServer
                ? toHashtable(smtpMailServer)
                : null;
    }

    public boolean deleteMailServer(final String authToken, final String mailServerId) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }


        Long mailServerIdLong = Long.valueOf(mailServerId);
        Map<Long, SMTPMailServer> mailServersMap = getMailServersMap();

        mailServersMap.remove(mailServerIdLong);
        persistMailServersMap(mailServersMap);

        return true;
    }

    public Hashtable<String, String> getMailServerIdsAndNames(final String authToken) throws RemoteException
    {
        if (!isCurrentUserAnAdmin())
        {
            throw new RemoteException("You do not have admin permission.");
        }


        Map<Long, SMTPMailServer> mailServersMap = getMailServersMap();
        Hashtable<String, String> idsToNamesMap = new Hashtable<>(mailServersMap.size());

        for (Map.Entry<Long, SMTPMailServer> e : mailServersMap.entrySet())
        {
            idsToNamesMap.put(e.getKey().toString(), e.getValue().getName());
        }

        return idsToNamesMap;
    }

    public String login(final String s, final String s1) throws RemoteException
    {
        return null;
    }

    public boolean logout(final String s) throws RemoteException
    {
        return false;
    }
}
