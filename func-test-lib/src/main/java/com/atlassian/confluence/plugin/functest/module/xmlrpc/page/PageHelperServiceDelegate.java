package com.atlassian.confluence.plugin.functest.module.xmlrpc.page;


import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import org.apache.commons.lang.StringUtils;

import java.util.Hashtable;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class PageHelperServiceDelegate implements PageHelperService
{
    private PermissionManager permissionManager;

    private PageManager pageManager;

    @SuppressWarnings("unused")
    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    @SuppressWarnings("unused")
    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    @Override
    public Map<String, ?> getPage(String authenticationToken, String pageId)
    {
        Page page = pageManager.getPage(Long.parseLong(pageId));
        Map<String, Object> pageStruct = null;

        if (null != page && permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(),  Permission.VIEW, page))
        {
            pageStruct = new Hashtable<>();

            pageStruct.put("id", page.getIdAsString());
            pageStruct.put("spaceKey", page.getSpaceKey());

            Page parentPage = page.getParent();
            if (null != parentPage)
                pageStruct.put("parentId", page.getParent().getIdAsString());

            pageStruct.put("title", page.getTitle());
            pageStruct.put("version", page.getVersion());
            pageStruct.put("content", page.getBodyAsString());

            final ConfluenceUser creator = page.getCreator();
            final String creatorName = creator != null ? creator.getName() : null;

            if (isNotBlank(creatorName))
                pageStruct.put("creator", creatorName);

            pageStruct.put("created", page.getCreationDate());

            final ConfluenceUser lastModifier = page.getLastModifier();
            final String lastModifierName = lastModifier != null ? lastModifier.getName() : null;

            if (isNotBlank(lastModifierName))
                pageStruct.put("lastModifier", lastModifierName);

            pageStruct.put("lastModified", page.getLastModificationDate());
        }

        return pageStruct;
    }

    @Override
    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    @Override
    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
