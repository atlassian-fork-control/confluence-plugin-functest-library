package com.atlassian.confluence.plugin.functest.module.xmlrpc.page;

import com.atlassian.confluence.rpc.RemoteException;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Map;

public class PageHelperServiceImpl implements PageHelperService
{
    private PageHelperService pageHelperServiceDelegate;

    private PlatformTransactionManager transactionManager;

    @SuppressWarnings("unused")
    public void setPageHelperServiceDelegate(PageHelperService pageHelperServiceDelegate)
    {
        this.pageHelperServiceDelegate = pageHelperServiceDelegate;
    }

    @SuppressWarnings("unused")
    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, ?> getPage(final String authenticationToken, final String pageId)
    {
        return (Map<String, ?>) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    @Override
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return pageHelperServiceDelegate.getPage(authenticationToken, pageId);
                    }
                }
        );
    }

    @Override
    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    @Override
    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
