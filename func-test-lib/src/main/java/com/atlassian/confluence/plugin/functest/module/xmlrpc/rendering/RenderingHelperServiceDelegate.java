package com.atlassian.confluence.plugin.functest.module.xmlrpc.rendering;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.wysiwyg.ConfluenceWysiwygConverter;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.apache.commons.lang.StringUtils;

import javax.xml.stream.XMLStreamException;

public class RenderingHelperServiceDelegate implements RenderingHelperService
{
    private ConfluenceWysiwygConverter confluenceWysiwygConverter;

    private ContentEntityManager contentEntityManager;

    private XhtmlContent xhtmlContent;

    @SuppressWarnings("unused")
    public void setConfluenceWysiwygConverter(ConfluenceWysiwygConverter confluenceWysiwygConverter)
    {
        this.confluenceWysiwygConverter = confluenceWysiwygConverter;
    }

    @SuppressWarnings("unused")
    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    @SuppressWarnings("unused")
    public void setXhtmlContent(XhtmlContent xhtmlContent)
    {
        this.xhtmlContent = xhtmlContent;
    }

    private ContentEntityObject getContentEntityObject(String pageId)
    {
        if (StringUtils.isNotBlank(pageId) && StringUtils.isNumeric(pageId))
        {
            return contentEntityManager.getById(Long.parseLong(pageId));
        }

        return null;
    }

    public String convertMarkupToXhtml(String authToken, String markup, String pageId)
    {
        return confluenceWysiwygConverter.convertWikiMarkupToXHtml(getContentEntityObject(pageId), markup);
    }

    public String convertXhtmlToMarkup(String authToken, String xhtml, String pageId)
    {
        return confluenceWysiwygConverter.convertXHtmlToWikiMarkup(getContentEntityObject(pageId), xhtml);
    }

    @Override
    public String getViewFormat(String authToken, String pageId) throws XMLStreamException, XhtmlException
    {
        ContentEntityObject contentEntityObject = getContentEntityObject(pageId);
        return null == contentEntityObject ? null : xhtmlContent.convertStorageToView(contentEntityObject.getBodyAsString(), new DefaultConversionContext(contentEntityObject.toPageContext()));
    }

    public String login(String s, String s1) throws RemoteException
    {
        return null;
    }

    public boolean logout(String s) throws RemoteException
    {
        return false;
    }
}
