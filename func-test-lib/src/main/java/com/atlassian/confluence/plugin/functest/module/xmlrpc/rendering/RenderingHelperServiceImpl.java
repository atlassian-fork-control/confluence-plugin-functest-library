package com.atlassian.confluence.plugin.functest.module.xmlrpc.rendering;

import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import org.apache.log4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.xml.stream.XMLStreamException;

public class RenderingHelperServiceImpl implements RenderingHelperService
{
    private static Logger LOG = Logger.getLogger(RenderingHelperService.class);


    private RenderingHelperService renderingHelperService;


    private PlatformTransactionManager transactionManager;

    @SuppressWarnings("unused")
    public void setRenderingHelperService(RenderingHelperService renderingHelperService)
    {
        this.renderingHelperService = renderingHelperService;
    }

    @SuppressWarnings("unused")
    public void setTransactionManager(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public String convertMarkupToXhtml(final String authToken, final String markup, final String pageId)
    {
        return (String) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return renderingHelperService.convertMarkupToXhtml(authToken, markup, pageId);
                    }
                }
        );
    }

    public String convertXhtmlToMarkup(final String authToken, final String xhtml, final String pageId)
    {
        return (String) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        return renderingHelperService.convertXhtmlToMarkup(authToken, xhtml, pageId);
                    }
                }
        );
    }

    @Override
    public String getViewFormat(final String authToken, final String pageId) throws XMLStreamException, XhtmlException
    {
        return (String) new TransactionTemplate(transactionManager).execute(
                new TransactionCallback()
                {
                    public Object doInTransaction(TransactionStatus transactionStatus)
                    {
                        try
                        {
                            return renderingHelperService.getViewFormat(authToken, pageId);
                        }
                        catch (Exception e)
                        {
                            LOG.error(String.format("Unable to render content %s to XHTML", pageId), e);
                            return null;
                        }
                    }
                }
        );
    }

    public String login(String s, String s1)
    {
        return null;
    }

    public boolean logout(String s)
    {
        return false;
    }
}
