package com.atlassian.confluence.plugin.functest.util;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Map;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Pair;
import com.atlassian.util.concurrent.LazyReference;

import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.apache.maven.settings.Mirror;
import org.apache.maven.settings.Profile;
import org.apache.maven.settings.Repository;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.apache.maven.settings.building.DefaultSettingsBuilderFactory;
import org.apache.maven.settings.building.DefaultSettingsBuildingRequest;
import org.apache.maven.settings.building.SettingsBuilder;
import org.apache.maven.settings.building.SettingsBuildingRequest;
import org.apache.maven.settings.building.SettingsBuildingResult;
import org.apache.maven.settings.crypto.DefaultSettingsDecrypter;
import org.apache.maven.settings.crypto.DefaultSettingsDecryptionRequest;
import org.apache.maven.settings.crypto.SettingsDecrypter;
import org.apache.maven.settings.crypto.SettingsDecryptionRequest;
import org.apache.maven.settings.crypto.SettingsDecryptionResult;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transfer.AbstractTransferListener;
import org.eclipse.aether.transfer.TransferCancelledException;
import org.eclipse.aether.transfer.TransferEvent;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.util.repository.AuthenticationBuilder;
import org.eclipse.aether.util.repository.DefaultMirrorSelector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonatype.plexus.components.cipher.DefaultPlexusCipher;
import org.sonatype.plexus.components.sec.dispatcher.DefaultSecDispatcher;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Provides some basic Maven functionality for use at runtime.
 *
 * @since 1.0.6
 */
@ExperimentalApi
public class EmbeddedMaven
{
    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedMaven.class);

    /**
     * This will load the ~/.m2/settings.xml file, and also decrypt any passwords if necessary.
     */
    private static final LazyReference<Settings> mavenSettings = new LazyReference<Settings>()
    {
        @Override
        protected Settings create() throws Exception
        {
            final File m2Home = new File(new File(System.getProperty("user.home")), "/.m2/");

            final SettingsBuildingRequest settingsBuildingRequest = new DefaultSettingsBuildingRequest();
            settingsBuildingRequest.setUserSettingsFile(new File(m2Home, "settings.xml"));

            final SettingsBuilder builder = new DefaultSettingsBuilderFactory().newInstance();
            final SettingsBuildingResult settingsBuildingResult = builder.build(settingsBuildingRequest);

            if (!settingsBuildingResult.getProblems().isEmpty())
            {
                throw settingsBuildingResult.getProblems().get(0).getException();
            }

            final Settings effectiveSettings = settingsBuildingResult.getEffectiveSettings();
            if (effectiveSettings.getLocalRepository() == null)
            {
                effectiveSettings.setLocalRepository(new File(m2Home, "/repository").getAbsolutePath());
            }

            // Decrypt passwords if needed.
            if (new File(m2Home, "/settings-security.xml").exists())
            {
                final DefaultSecDispatcher secDispatcher = new DefaultSecDispatcher()
                {
                    {
                        _cipher = new DefaultPlexusCipher();
                        // DefaultSecDispatcher code expands the tilde into proper home dir path.
                        _configurationFile = "~/.m2/settings-security.xml";
                    }
                };
                final SettingsDecrypter settingsDecrypter = new DefaultSettingsDecrypter();
                final Field securityDispatcherField = DefaultSettingsDecrypter.class.getDeclaredField("securityDispatcher");
                securityDispatcherField.setAccessible(true);
                securityDispatcherField.set(settingsDecrypter, secDispatcher);
                final SettingsDecryptionRequest decryptionRequest = new DefaultSettingsDecryptionRequest(effectiveSettings);
                final SettingsDecryptionResult decryptionResult = settingsDecrypter.decrypt(decryptionRequest);

                if (!decryptionResult.getProblems().isEmpty())
                {
                    throw decryptionResult.getProblems().get(0).getException();
                }

                effectiveSettings.setServers(decryptionResult.getServers());
                effectiveSettings.setProxies(decryptionResult.getProxies());
            }

            return effectiveSettings;
        }
    };

    private static final LazyReference<Pair<RepositorySystem, RepositorySystemSession>> aether = new LazyReference<Pair<RepositorySystem, RepositorySystemSession>>()
    {
        @Override
        protected Pair<RepositorySystem, RepositorySystemSession> create() throws Exception
        {
            // Ensure Aether can find the needed components.
            // ServiceLocator is preferable here rather than starting up a full DI context.
            // Also, Maven / Aether uses sisu-guice, which stomps on regular Guice pretty bad.
            final DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
            locator.addService( RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class );
            locator.addService( TransporterFactory.class, HttpTransporterFactory.class );

            final RepositorySystem system = locator.getService(RepositorySystem.class);
            final DefaultRepositorySystemSession session = MavenRepositorySystemUtils.newSession();

            final Settings settings = mavenSettings.get();
            final LocalRepository repo = new LocalRepository(settings.getLocalRepository());
            session.setLocalRepositoryManager(system.newLocalRepositoryManager(session, repo));

            final DefaultMirrorSelector mirrorSelector = new DefaultMirrorSelector();
            for (final Mirror m : settings.getMirrors())
            {
                mirrorSelector.add(m.getId(), m.getUrl(), m.getLayout(), false, m.getMirrorOf(), m.getMirrorOfLayouts());
            }
            session.setMirrorSelector(mirrorSelector);

            session.setTransferListener(new AbstractTransferListener()
            {
                @Override
                public void transferInitiated(final TransferEvent event) throws TransferCancelledException
                {
                    LOG.info("Transferring artifact {}", event.getResource());
                }
            });

            return Pair.pair(system, session);
        }
    };

    /**
     * Resolves an artifact into the local Maven repository.
     *
     * @throws RuntimeException if resolution fails for any reason, or artifact doesn't exist.
     *
     * @param gav co-ordinates for the artifact to resolve
     */
    public static File resolveDep(String gav)
    {
        ArtifactRequest request = new ArtifactRequest();
        request.setArtifact(new DefaultArtifact(gav));

        final Settings settings = mavenSettings.get();
        final Pair<RepositorySystem, RepositorySystemSession> pair = aether.get();

        Map<String, Repository> mavenRepos = newHashMap();
        for (final Profile profile : settings.getProfiles())
        {
            for (final Repository repository : profile.getRepositories())
            {
                mavenRepos.put(repository.getId(), repository);
            }
        }

        for (final Repository repository : mavenRepos.values())
        {
            if (!repository.getUrl().startsWith("http"))
            {
                continue;
            }

            final RemoteRepository.Builder remoteRepoBuilder = new RemoteRepository.Builder(repository.getId(), "default", repository.getUrl());

            RemoteRepository remoteRepo = remoteRepoBuilder.build();
            final RemoteRepository mirror = pair.right().getMirrorSelector().getMirror(remoteRepo);
            remoteRepo = mirror != null ? mirror : remoteRepo;

            final RemoteRepository.Builder remoteRepoBuilder2 = new RemoteRepository.Builder(remoteRepo.getId(), "default", remoteRepo.getUrl());

            final Server server = settings.getServer(remoteRepo.getId());
            if (server != null && server.getUsername() != null && server.getPassword() != null) {
                remoteRepoBuilder2.setAuthentication(new AuthenticationBuilder()
                        .addUsername(server.getUsername())
                        .addPassword(server.getPassword())
                        .build());
            }
            request.addRepository(remoteRepoBuilder2.build());
        }

        try
        {
            final ArtifactResult artifactResult = pair.left().resolveArtifact(pair.right(), request);
            if (artifactResult.isMissing())
            {
                LOG.info("Failed to resolve Maven artifact {}", gav);
            }
            else
            {
                return artifactResult.getArtifact().getFile();
            }
        }
        catch (ArtifactResolutionException e)
        {
            LOG.info("Failed to resolve Maven artifact {}, {}", gav, e);
        }

        return null;
    }
}