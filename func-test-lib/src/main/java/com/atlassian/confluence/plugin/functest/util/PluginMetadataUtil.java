package com.atlassian.confluence.plugin.functest.util;


import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PluginMetadataUtil
{
    private static XPathExpression createXPathExpression(String xpathExpression) throws XPathExpressionException
    {
        return XPathFactory.newInstance().newXPath().compile(xpathExpression);
    }

    public static Document getPluginDescriptorDom(File pluginJar) throws IOException, ParserConfigurationException, SAXException
    {
        ZipInputStream zipInputStream = null;

        try
        {
            ZipEntry ze;
            zipInputStream = new ZipInputStream(new FileInputStream(pluginJar));

            while (null != (ze = zipInputStream.getNextEntry()))
            {
                if (ze.getName().equalsIgnoreCase("atlassian-plugin.xml"))
                {
                    DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    return documentBuilder.parse(zipInputStream);
                }
            }
        }
        finally
        {
            IOUtils.closeQuietly(zipInputStream);
        }

        return null;
    }

    public static String getPluginName(Document pluginDescriptor) throws XPathExpressionException
    {
        XPathExpression xPathExpression = createXPathExpression("//atlassian-plugin/@name");
        return xPathExpression.evaluate(pluginDescriptor.getDocumentElement());
    }

    public static String getPluginKey(Document pluginDescriptor) throws XPathExpressionException
    {
        XPathExpression xPathExpression = createXPathExpression("//atlassian-plugin/@key");
        return xPathExpression.evaluate(pluginDescriptor.getDocumentElement());
    }
}
