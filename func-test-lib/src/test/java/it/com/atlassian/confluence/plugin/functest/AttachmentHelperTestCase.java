package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class AttachmentHelperTestCase extends AbstractConfluencePluginWebTestCase
{
    private long idOfPageToContainAttachments;

    private static int spaceIndex = 0;

    protected void setUp() throws Exception
    {
        super.setUp();

        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey("ds" + spaceIndex++);
        spaceHelper.setName("Test Space");
        spaceHelper.setDescription("Test Space");

        assertTrue(spaceHelper.create());

        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(spaceHelper.getKey());
        pageHelper.setTitle("Test Attachment Page");
        pageHelper.setContent(StringUtils.EMPTY);

        assertTrue(pageHelper.create());

        idOfPageToContainAttachments = pageHelper.getId();
    }

    private byte[] getLicenseResourceBytes() throws IOException
    {
        InputStream in = null;
        ByteArrayOutputStream byteArrayOutputStream = null;

        try
        {
            /* Eval license */
            in = getClass().getClassLoader().getResourceAsStream("license.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();

            IOUtils.copy(in, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();

        }
        finally
        {
            IOUtils.closeQuietly(byteArrayOutputStream);
            IOUtils.closeQuietly(in);
        }
    }

    public void testCreateAttachment() throws IOException
    {
        final AttachmentHelper attachmentHelper = getAttachmentHelper();
        final AttachmentHelper anotherAttachmentHelper;
        final byte[] licenseBytes = getLicenseResourceBytes();

        attachmentHelper.setParentId(idOfPageToContainAttachments);
        attachmentHelper.setFileName("license.txt");
        attachmentHelper.setContentType("text/plain");
        attachmentHelper.setContentLength(licenseBytes.length);
        attachmentHelper.setContent(licenseBytes);

        assertTrue(attachmentHelper.create());


        anotherAttachmentHelper = getAttachmentHelper(
                attachmentHelper.getParentId(),
                attachmentHelper.getFileName());

        assertTrue(anotherAttachmentHelper.read());
        assertTrue(Arrays.equals(licenseBytes, anotherAttachmentHelper.getContent()));
    }

    public void testUpdateAttachment() throws IOException
    {
        final AttachmentHelper attachmentHelper;
        final AttachmentHelper anotherAttachmentHelper;
        final byte[] newContent = "newContent".getBytes("UTF-8");

        testCreateAttachment(); /* Cheap way to repeat the attachment creation process */

        attachmentHelper = getAttachmentHelper();

        attachmentHelper.setParentId(idOfPageToContainAttachments);
        attachmentHelper.setFileName("license.txt");
        attachmentHelper.setContentType("text/plain");
        attachmentHelper.setContentLength(newContent.length);
        attachmentHelper.setContent(newContent);

        assertTrue(attachmentHelper.update());

        anotherAttachmentHelper = getAttachmentHelper(
                attachmentHelper.getParentId(),
                attachmentHelper.getFileName());

        assertTrue(anotherAttachmentHelper.read());
        assertTrue(Arrays.equals(newContent, anotherAttachmentHelper.getContent()));
    }

    public void testReadAttachmentDetails() throws IOException
    {
        final AttachmentHelper attachmentHelper;
        final byte[] licenseBytes = getLicenseResourceBytes();

        testCreateAttachment(); /* Cheap way to repeat the attachment creation process */

        attachmentHelper = getAttachmentHelper(idOfPageToContainAttachments, "license.txt");
        assertTrue(attachmentHelper.read());

        assertEquals(idOfPageToContainAttachments, attachmentHelper.getParentId());
        assertEquals("text/plain", attachmentHelper.getContentType());
        assertEquals(licenseBytes.length, attachmentHelper.getContentLength());
        assertEquals("license.txt", attachmentHelper.getFileName());
        assertEquals(getConfluenceWebTester().getCurrentUserName(), attachmentHelper.getCreator());

        assertTrue(Arrays.equals(licenseBytes, attachmentHelper.getContent()));
    }

    public void testDeleteAttachment() throws IOException
    {
        final AttachmentHelper attachmentHelper;
        testCreateAttachment(); /* Cheap way to repeat the attachment creation process */

        attachmentHelper = getAttachmentHelper(idOfPageToContainAttachments, "license.txt");
        assertTrue(attachmentHelper.delete());
    }
}
