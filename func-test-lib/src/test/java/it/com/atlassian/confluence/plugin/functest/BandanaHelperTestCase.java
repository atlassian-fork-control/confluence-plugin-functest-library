package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.BandanaHelper;

public class BandanaHelperTestCase extends AbstractConfluencePluginWebTestCase
{

    private BandanaHelper bandanaHelper;

    protected void setUp() throws Exception
    {
        super.setUp();
        bandanaHelper = getBandanaHelper();
    }

    public void testSpaceBandanaValueNotRetrievableIfGlobalContextSpecified()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey("TST");
        bandanaHelper.setBandanaKey("testSpaceBandanaValueNotRetrievableIfGlobalContextSpecified");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testSpaceBandanaValueNotRetrievableIfGlobalContextSpecified");

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertNull(anotherBandanaHelper.getBandanaKey());
        assertNull(anotherBandanaHelper.getValue());
    }

    public void testCreateGlobalBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey(null);
        bandanaHelper.setBandanaKey("testCreateGlobalBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testCreateGlobalBandanaValue");

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertEquals("testCreateGlobalBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());
    }

    public void testUpdateGlobalBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey(null);
        bandanaHelper.setBandanaKey("testUpdateGlobalBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testUpdateGlobalBandanaValue");

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertEquals("testUpdateGlobalBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());

        bandanaHelper.setValue("false");
        assertTrue(bandanaHelper.update());


        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertEquals("testUpdateGlobalBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("false", anotherBandanaHelper.getValue());
    }

    public void testDeleteGlobalBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey(null);
        bandanaHelper.setBandanaKey("testDeleteGlobalBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testDeleteGlobalBandanaValue");

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertEquals("testDeleteGlobalBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());

        assertTrue(bandanaHelper.delete());

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertNull(anotherBandanaHelper.getBandanaKey());
        assertNull(anotherBandanaHelper.getValue());
    }

    public void testCreateSpaceBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey("testSpaceKey");
        bandanaHelper.setBandanaKey("testCreateSpaceBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testCreateSpaceBandanaValue");
        anotherBandanaHelper.setSpaceKey("testSpaceKey");

        assertTrue(anotherBandanaHelper.read());

        assertEquals("testSpaceKey", anotherBandanaHelper.getSpaceKey());
        assertEquals("testCreateSpaceBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());
    }

    public void testUpdateSpaceBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey("testSpaceKey");
        bandanaHelper.setBandanaKey("testUpdateSpaceBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testUpdateSpaceBandanaValue");
        anotherBandanaHelper.setSpaceKey("testSpaceKey");

        assertTrue(anotherBandanaHelper.read());

        assertEquals("testSpaceKey", anotherBandanaHelper.getSpaceKey());
        assertEquals("testUpdateSpaceBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());

        bandanaHelper.setValue("false");
        assertTrue(bandanaHelper.update());


        assertTrue(anotherBandanaHelper.read());

        assertEquals("testSpaceKey", anotherBandanaHelper.getSpaceKey());
        assertEquals("testUpdateSpaceBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("false", anotherBandanaHelper.getValue());
    }

    public void testDeleteSpaceBandanaValue()
    {
        final BandanaHelper anotherBandanaHelper;

        bandanaHelper.setSpaceKey("testSpaceKey");
        bandanaHelper.setBandanaKey("testDeleteSpaceBandanaValue");
        bandanaHelper.setValue("true");

        assertTrue(bandanaHelper.create());

        anotherBandanaHelper = getBandanaHelper("testDeleteSpaceBandanaValue");
        anotherBandanaHelper.setSpaceKey("testSpaceKey");

        assertTrue(anotherBandanaHelper.read());

        assertEquals("testSpaceKey", anotherBandanaHelper.getSpaceKey());
        assertEquals("testDeleteSpaceBandanaValue", anotherBandanaHelper.getBandanaKey());
        assertEquals("true", anotherBandanaHelper.getValue());

        assertTrue(bandanaHelper.delete());

        assertTrue(anotherBandanaHelper.read());

        assertNull(anotherBandanaHelper.getSpaceKey());
        assertNull(anotherBandanaHelper.getBandanaKey());
        assertNull(anotherBandanaHelper.getValue());
    }
}
