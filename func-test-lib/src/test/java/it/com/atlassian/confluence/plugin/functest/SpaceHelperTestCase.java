package it.com.atlassian.confluence.plugin.functest;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;


public class SpaceHelperTestCase extends AbstractConfluencePluginWebTestCase {

    public void testCreateGlobalSpace() {
        final SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey("TST");
        spaceHelper.setName("Test Space");
        spaceHelper.setDescription("Test Space Description.");
        spaceHelper.setType(SpaceHelper.TYPE_GLOBAL);

        assertTrue(spaceHelper.create());

        gotoPage("/spaces/viewspacesummary.action?key=" + spaceHelper.getKey());
        assertTextPresent(spaceHelper.getName());
        assertTextPresent(spaceHelper.getDescription());

        assertTrue(spaceHelper.delete());
    }

//      Commented because there is a problem with Confluence's storeSpace method. It seems to assume that we *always*
//      want to create a space
//    public void testUpdateSpace() {
//        final SpaceHelper spaceHelper = getSpaceHelper();
//
//        spaceHelper.setKey("TST");
//        spaceHelper.setName("Test Space");
//        spaceHelper.setDescription("Test Space Description.");
//        spaceHelper.setType(SpaceHelper.TYPE_GLOBAL);
//
//        assertTrue(spaceHelper.create());
//
//        gotoPage("/spaces/viewspacesummary.action?key=" + spaceHelper.getKey());
//        assertTextPresent(spaceHelper.getName());
//        assertTextPresent(spaceHelper.getDescription());
//
//        spaceHelper.setName("Edited Test Space");
//        spaceHelper.setDescription("Edited Space Description.");
//
//        assertTrue(spaceHelper.updateMailServer());
//
//        gotoPage("/spaces/viewspacesummary.action?key=" + spaceHelper.getKey());
//        assertTextPresent(spaceHelper.getName());
//        assertTextPresent(spaceHelper.getDescription());
//
//        assertTrue(spaceHelper.delete());
//    }

    public void testReadSpace() {
        final PageHelper pageHelper;
        final SpaceHelper spaceHelper = getSpaceHelper("ds");
        final long spaceHomePageId;

        assertTrue(spaceHelper.read());

        assertEquals("Demonstration Space", spaceHelper.getName());
        assertEquals("A space which demonstrates Confluence functionality.", spaceHelper.getDescription());
        assertTrue((spaceHomePageId = spaceHelper.getHomePageId()) > 0);

        pageHelper = getPageHelper(spaceHomePageId);
        assertTrue(pageHelper.read());

        assertEquals("Confluence Overview", pageHelper.getTitle());
    }
}
